// JSON Server module
const jsonServer = require("json-server");
const server = jsonServer.create();
const router = jsonServer.router("db/db.json");
const middlewares = jsonServer.defaults();

server.use(middlewares);
// Add this before server.use(router)
server.use(
  // Add custom route here if needed
  jsonServer.rewriter({
    "/api/*": "/$1",
  })
);
server.use(router);

// GET https://www.googleapis.com/youtube/v3/commentThreads
// const videoId = "YOUR_VIDEO_ID"; // Replace with the actual video ID
// const apiKey = "YOUR_API_KEY"; // Replace with your YouTube Data API key
// const url = `https://www.googleapis.com/youtube/v3/commentThreads?part=snippet&videoId=${videoId}&maxResults=100&key=${apiKey}`;
// fetch(url)
//   .then((response) => response.json())
//   .then((data) => {
//     // Process the response data here
//     console.log(data);
//   })
//   .catch((error) => {
//     // Handle any errors that occur during the request
//     console.error("Error:", error);
//   });

// Use the apiKey variable in your code
const apiKey = process.env.API_KEY;
console.log("API Key:", apiKey);

server.listen(3000, () => {
  console.log("JSON Server is running");
});

// Export the Server API
module.exports = server;
